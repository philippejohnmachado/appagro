import { Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
const d = Dimensions.get("window");
const imageHeight = Math.round((d.width * 9) / 20);
const imageWidth = d.width;

export default {
  basePadding: 20,
  baseMargin: 15,
  baseRadius: 5,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  h: imageHeight,
  w: imageWidth
};
