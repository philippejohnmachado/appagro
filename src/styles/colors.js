export default {
  white: "#FFF",
  lighter: "#EEE",
  light: "#DDD",
  lightgray: "#B5B5B5",
  regular: "#999",
  dark: "#666",
  darker: "#333",
  black: "#000",

  primary: "#006400",
  secundary: "#FFFFFF",
  success: "#32D25D",
  danger: "#E37A7A",

  transparent: "transparent",
  darkTransparent: "rgba(0, 0, 0, 0.6)",
  whiteTransparent: "rgba(255, 255, 255, 0.3)"
};
