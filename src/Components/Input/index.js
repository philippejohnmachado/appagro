import React, { forwardRef } from "react";
import PropTypes from "prop-types";
// import Icon from "react-native-vector-icons/MaterialIcons";
import IonIcons from "react-native-vector-icons/FontAwesome";
import { Container, TInput } from "./styles";
import { colors } from "~/styles";

IonIcons.loadFont();

function Input({ style, icon, ...rest }, ref) {
  return (
    <Container style={style}>
      {icon && <IonIcons name={icon} size={20} color={colors.black} />}
      <TInput {...rest} ref={ref} />
    </Container>
  );
}

Input.propTypes = {
  icon: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
};

Input.defaultProps = {
  icon: null,
  style: {}
};

export default forwardRef(Input);
