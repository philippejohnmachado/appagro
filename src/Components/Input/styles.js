import styled from "styled-components/native";
import { colors } from "~/styles";

export const Container = styled.View`
  padding: 0 15px;
  margin-left: 5px;
  margin-right:5px;
  height: 46px;
  background: rgba(0, 0, 0, 0.1);
  border-radius: 20px;
  flex-direction: row;
  align-items: center;
  /* border-width: 1; */
`;

export const TInput = styled.TextInput.attrs({
  placeholderTextColor: colors.black
})`
  flex: 1;
  font-size: 15px;
  margin-left: 10px;
  color: #000;
`;
