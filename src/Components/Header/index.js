import React, { Component } from "react";
import { View, Text } from "react-native";
import IonIcons from "react-native-vector-icons/FontAwesome";
import styles from "./styles";
import { colors } from "~/styles";

export default class Header extends Component {
  render() {
    return (
      <View style={styles.header}>
        <IonIcons
          name="bars"
          color={colors.primary}
          size={28}
          style={styles.icon}
          onPress={() => this.props.navigation.toggleDrawer()}
        />
        <Text style={styles.title}>{this.props.Text}</Text>
      </View>
    );
  }
}
