import { StyleSheet } from "react-native";
import { colors } from "~/styles";

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    height: 60,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    backgroundColor: colors.white,
    zIndex: 10,
    justifyContent: "center",
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 1.0,
    shadowRadius: 5
  },
  icon: {
    zIndex: 9,
    position: "absolute",
    top: 15,
    left: 20
  },
  title: {
    color: colors.primary,
    top: 15,
    fontSize: 20,
    position: "absolute",
    justifyContent: "center"
  }
});

export default styles;
