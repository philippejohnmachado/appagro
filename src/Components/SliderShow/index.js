import React, { Component } from "react";
import { Image, ScrollView, Dimensions } from "react-native";
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp
// } from "react-native-responsive-screen";

const dimensions = Dimensions.get("window");
// const imageHeight = Math.round((dimensions.width * 9) / 20);
const imageWidth = dimensions.width;
import styles from "./styles";

export default class HomeActivity extends Component {
  componentDidMount() {
    const numOfBackground = 4;
    let scrollValue = 0,
      scrolled = 0;
    setInterval(function() {
      scrolled++;
      if (scrolled < numOfBackground) scrollValue = scrollValue + imageWidth;
      else {
        scrollValue = 0;
        scrolled = 0;
      }
      _scrollView.scrollTo({ x: scrollValue, animated: true });
    }, 7000);
  }

  render() {
    return (
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        ref={scrollView => {
          _scrollView = scrollView;
        }}
        pagingEnabled={false}
      >
        <Image
          style={styles.image}
          resizeMode="contain"
          source={require("~/assets/maquinas.jpg")}
        />
        <Image
          style={styles.image}
          resizeMode="contain"
          source={require("~/assets/maquinas.jpg")}
        />
      </ScrollView>
    );
  }
}
