import { StyleSheet } from "react-native";
import { metrics } from "~/styles";

const styles = StyleSheet.create({
  image: {
    height: metrics.h,
    width: metrics.w
  }
});

export default styles;
