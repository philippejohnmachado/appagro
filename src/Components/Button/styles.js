import styled from "styled-components/native";
import { RectButton } from "react-native-gesture-handler";

export const Container = styled(RectButton)`
  padding: 0 15px;
  height: 45px;
  margin-left: 5px;
  margin-right: 5px;
  background: rgba(0, 0, 0, 0.1);
  border-radius: 20px;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.Text`
  color: #000;
  font-weight: bold;
  font-size: 16px;
`;
