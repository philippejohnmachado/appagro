import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  // Image,
  // Dimensions,
  TouchableOpacity
} from "react-native";
import IonIcons from "react-native-vector-icons/FontAwesome";
import styles from "./styles";
import { colors } from "~/styles";
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp
// } from "react-native-responsive-screen";
// const WIDTH = Dimensions.get("window").width;
// const HEIGTH = Dimensions.get("window").height;

class SideMenu extends Component {
  navLink(nav, Icon, text) {
    return (
      <TouchableOpacity
        style={{ height: 50 }}
        onPress={() => this.props.navigation.navigate(nav)}
      >
        <View style={styles.linemenu}>
          <IonIcons
            name={Icon}
            color={colors.white}
            size={32}
            style={styles.icon}
          />
          <Text style={styles.link}>{text}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scroller}>
          <View style={styles.topLinks}>
            {/* <View
              style={{
                width: wp("100%"),
                height: hp("100%"),
                alignItems: "center",
                justifyContent: "center"
              }}
            > */}
            <View style={styles.profile}>
              <View style={styles.imgView}>
                {/* <Image
                  style={styles.thumbnail}
                  source={{ uri:}}
                /> */}
                <IonIcons
                  name="user-circle"
                  color={colors.white}
                  size={50}
                  // style={styles.}
                />
              </View>
              <View style={styles.profileText}>
                <Text style={styles.name}>User Name</Text>
              </View>
            </View>
            {/* </View> */}
          </View>
          <View style={styles.bottomLinks}>
            {this.navLink("Home", "home", "Home")}
            {this.navLink("Usuario", "user", "Usuario")}
          </View>
        </ScrollView>
        <View style={styles.footer}>
          <Text style={styles.description}>MachadoIT-AgroInfo</Text>
          <Text style={styles.version}>v00.01</Text>
        </View>
      </View>
    );
  }
}

export default SideMenu;
