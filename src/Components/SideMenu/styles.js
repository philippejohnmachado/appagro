import { StyleSheet } from "react-native";
import { colors } from "~/styles";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightgray
  },
  scroller: {
    flex: 1
  },
  profile: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 25,
    borderBottomWidth: 1,
    borderBottomColor: colors.white
  },
  profileText: {
    flex: 3,
    flexDirection: "column",
    justifyContent: "center"
  },
  name: {
    fontSize: 20,
    paddingBottom: 5,
    color: "white",
    textAlign: "left"
  },
  imgView: {
    paddingLeft: 20,
    paddingRight: 20
  },
  img: {
    height: 70,
    width: 70,
    borderRadius: 50
  },
  topLinks: {
    height: 120,
    flex: 1,
    // backgroundColor: colors.primary
  },
  bottomLinks: {
    flex: 1,
    // backgroundColor: colors.primary,
    paddingTop: 10,
    paddingBottom: 450
  },
  icon: {
    marginLeft: 30,
    marginTop: 5
  },
  linemenu: {
    flexDirection: "row"
  },
  link: {
    flex: 1,
    fontSize: 15,
    padding: 6,
    paddingLeft: 10,
    margin: 5,
    textAlign: "left",
    color: colors.white
  },
  footer: {
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colors.white,
    borderTopWidth: 1,
    borderTopColor: colors.lightgray
  },
  version: {
    flex: 1,
    textAlign: "right",
    marginRight: 20,
    fontSize: 10,
    color: colors.dark
  },
  description: {
    flex: 1,
    textAlign: "left",
    marginLeft: 20,
    fontSize: 10,
    color: colors.dark
  }
});

export default styles;
