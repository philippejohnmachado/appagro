import React, { Component } from "react";
import { View, Text, TextInput, TouchableOpacity, Alert } from "react-native";
import { colors } from "~/styles";
import Crypto from "~/utils/crypto";
import api from "~/services/api";

import {
  Container,
  Form,
  FormInput,
  SubmitButton,
  // SignLink,
  // SignLinkText
} from "./styles";

export default class Cadastro extends Component {
  state = {
    nome: "",
    sobreNome: "",
    cpf: "",
    email: "",
    senha: "",
    confirmarSenha: ""
  };

  async componentDidMount() {
    this.nome = "";
    this.sobreNome = "";
    this.cpf = "";
    this.email = "";
    this.senha = "";
    this.confirmarSenha = "";
  }

  handleNome = text => {
    this.setState({ nome: text });
  };

  handleSobrenome = text => {
    this.setState({ sobreNome: text });
  };

  handleCpf = text => {
    this.setState({ cpf: text });
  };

  handleEmail = text => {
    this.setState({ email: text });
  };

  handleSenha = text => {
    this.setState({ senha: text });
  };

  handleConfirmarSenha = text => {
    this.setState({ confirmarSenha: text });
  };

  Cadastro = async (nome, sobreNome, cpf, email, senha, confirmarSenha) => {
    try {
      if (senha === confirmarSenha) {
        var resultCrypto = await Crypto(senha);

        const response = await api.post("/user", {
          nome: nome,
          sobrenome: sobreNome,
          cpf: cpf,
          email: email,
          senha: resultCrypto
        });

        const { result, error, message } = response.data;
        if (result && !error) {
          this.props.navigation.navigate("Home");
        } else {
          Alert.alert("Ops!", message);
        }
      } else {
        Alert.alert("Ops!", "Senha nao confere");
      }
    } catch (e) {
      Alert.alert("Ops!", "error: " + e);
    }
  };

  render() {
    return (
      <Container>
        <View style={{ alignItems: "center", marginTop: 10, fontSize: 16 }}>
          <Text>Registar</Text>
        </View>
        <Form>
          <FormInput
            placeholder="Nome"
            fontSize={12}
            returnKeyType="next"
            autoCapitalize="none"
            autoCorrect={false}
            onSubmitEditing={() => this.sobrenomeInput.focus()}
            onChangeText={this.handleNome}
            placeholderTextColor={colors.black}
          />
          <FormInput
            placeholder="Sobrenome"
            fontSize={12}
            returnKeyType="next"
            autoCapitalize="none"
            autoCorrect={false}
            onSubmitEditing={() => this.cpfInput.focus()}
            onChangeText={this.handleSobrenome}
            placeholderTextColor={colors.black}
          />
          <FormInput
            placeholder="CPF"
            fontSize={12}
            returnKeyType="next"
            autoCapitalize="none"
            autoCorrect={false}
            onSubmitEditing={() => this.emailInput.focus()}
            onChangeText={this.handleCpf}
            placeholderTextColor={colors.black}
          />
          <FormInput
            placeholder="E-mail"
            fontSize={12}
            returnKeyType="next"
            autoCapitalize="none"
            autoCorrect={false}
            onSubmitEditing={() => this.senhaInput.focus()}
            onChangeText={this.handleEmail}
            placeholderTextColor={colors.black}
          />
          <FormInput
            placeholder="Senha"
            secureTextEntry={true}
            fontSize={12}
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={false}
            onSubmitEditing={() => this.confirmarSenhaInput.focus()}
            onChangeText={this.handleSenha}
            placeholderTextColor={colors.black}
          />
          <FormInput
            placeholder="Confirmar senha"
            secureTextEntry={true}
            fontSize={12}
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={this.handleConfirmarSenha}
            placeholderTextColor={colors.black}
          />
          <SubmitButton
            onPress={() =>
              this.Cadastro(
                this.state.nome,
                this.state.sobreNome,
                this.state.cpf,
                this.state.email,
                this.state.senha,
                this.state.confirmarSenha
              )
            }
          >
            <Text>Finalizar</Text>
          </SubmitButton>
        </Form>
      </Container>
    );
  }
}
