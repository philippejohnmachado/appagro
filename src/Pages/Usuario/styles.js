import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#C0C0C0",
    justifyContent: "center"
  }
});

export default styles;
