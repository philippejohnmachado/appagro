import React, { Component } from "react";
import { View, Text, FlatList, Image, Dimensions } from "react-native";
import styles from "./styles";
// import IonIcons from "react-native-vector-icons/FontAwesome";
import Header from "~/Components/Header";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

// const dimensions = Dimensions.get("window");
// const imageHeight = Math.round((dimensions.width * 9) / 15);
// const imageWidth = dimensions.width;

export default class Home extends Component {
  static navigationOptions = { header: null };
  state = {
    text: "Usuario"
  };

  render() {
    return (
      <View style={styles.container}>
        <Header Text={this.state.text} navigation={this.props.navigation} />
        <View style={{ justifyContent: "center" }}>
          <View
            style={{
              marginBottom: 30,
              backgroundColor: "#fff",
              height: hp("40%"),
              width: wp("40%"),
              marginTop: 30,
              justifyContent: "center",
              borderRadius: 15,
              shadowColor: "#000000",
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 1.0,
              shadowRadius: 5
            }}
          ></View>
        </View>
      </View>
    );
  }
}
