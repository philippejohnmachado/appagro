import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from "react-native";
import {
  Container,
  Form,
  FormInput,
  SubmitButton
  // SignLink,
  // SignLinkText
} from "./styles";
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp
// } from "react-native-responsive-screen";
import api from "~/services/api";
import Crypto from "~/utils/crypto";

export default class Cadastro extends Component {
  state = {
    senha: "",
    ConfirmarSenha: ""
  };

  async componentDidMount() {
    this.senha = "";
    this.ConfirmarSenha = "";
  }

  handlePassword = text => {
    this.setState({ senha: text });
  };

  handleNewPassword = text => {
    this.setState({ ConfirmarSenha: text });
  };

  NovaSenha = async (senha, NovaSenha) => {
    const data = await AsyncStorage.getItem("DATA_USER");
    console.tron.log(data, "DATA");
    const obj = JSON.parse(data);
    console.tron.log(obj, "OBJ");
    const { UserId, token, message, SenhaProv, result, error } = obj;

    if (senha === NovaSenha) {
      var resultCrypto = await Crypto(senha);

      const response = await api.post("/user/novaSenha/", {
        UserId: UserId.toString(),
        token: token,
        senha: resultCrypto
      });
      console.tron.log(response, "RESPONSE");
      const { message, result } = response.data;
      console.tron.log(message, "MSN");
      console.tron.log(result, "RSLT");
      if (message === "Senha Atualizada com sucesso" && result) {
        this.props.navigation.navigate("Home");
      }
    } else {
      Alert.alert("senhas nao confere");
    }
  };

  render() {
    return (
      <Container>
        <Text
          style={{
            textAlign: "center",
            fontSize: 20,
            color: "#000",
            fontWeight: "bold",
            marginTop: 10
          }}
        >
          Registrar nova senha
        </Text>
        <Form>
          <FormInput
            placeholder="Senha"
            fontSize={12}
            secureTextEntry={true}
            returnKeyType="next"
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={this.handleNewPassword}
            value={this.state.text}
            ref={senha => (this.senhaInput = senha)}
            placeholderTextColor="#000000"
          />
          <FormInput
            placeholder="Confirmar senha"
            fontSize={12}
            secureTextEntry={true}
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={this.handlePassword}
            value={this.state.text}
            ref={senha => (this.senhaInput = senha)}
            placeholderTextColor="#000000"
          />
          <SubmitButton
            onPress={() =>
              this.NovaSenha(this.state.senha, this.state.ConfirmarSenha)
            }
          >
            <Text>Finalizar</Text>
          </SubmitButton>
        </Form>
      </Container>
    );
  }
}
