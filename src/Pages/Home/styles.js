import { StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { colors } from "~/styles";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightgray,
    justifyContent: "center"
  },
  conteudo: {
    marginTop: 10,
    backgroundColor: colors.white,
    height: hp("25%"),
    width: wp("100%"),
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 1.0,
    shadowRadius: 5
  }
});

export default styles;
