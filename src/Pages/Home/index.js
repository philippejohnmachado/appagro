import React, { Component } from "react";
import { View, Dimensions, ScrollView, Image } from "react-native";
import styles from "./styles";
// import IonIcons from "react-native-vector-icons/FontAwesome";
import Header from "~/Components/Header";
import SliderShow from "~/Components/SliderShow";
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp
// } from "react-native-responsive-screen";

// const dimensions = Dimensions.get("window");
// const imageHeight = Math.round((dimensions.width * 9) / 15);
// const imageWidth = dimensions.width;

export default class Home extends Component {
  static navigationOptions = {
    header: null
  };

  state = {
    text: "Home"
  };

  render() {
    return (
      <View style={styles.container}>
        <Header Text={this.state.text} navigation={this.props.navigation} />
        <ScrollView style={{ paddingTop: 50 }}>
          <SliderShow />
          <View style={styles.conteudo}></View>
          <View style={styles.conteudo}></View>
          <View style={styles.conteudo}></View>
          <View style={styles.conteudo}></View>
        </ScrollView>
      </View>
    );
  }
}
