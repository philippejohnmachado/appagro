import React, { Component } from "react";
import {
  View,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Text,
  Alert,
  ActivityIndicator,
  ImageBackground,
  KeyboardAvoidingView,
  AsyncStorage
} from "react-native";
import {
  Container,
  Form,
  FormInput,
  SubmitButton,
  SignLink,
  SignLinkText
} from "./styles";
import IonIcons from "react-native-vector-icons/FontAwesome";
// import I18n from "~/locales/i18n";
// import { colors } from "~/styles";
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp
// } from "react-native-responsive-screen";
import api from "~/services/api";
import Crypto from "~/utils/crypto";
import Background from "~/Components/Background";

export default class Login extends Component {
  state = {
    email: "",
    password: ""
  };

  async componentDidMount() {
    this.email = "";
    this.senha = "";
  }

  handleEmail = text => {
    this.setState({ email: text });
  };

  handlePassword = text => {
    this.setState({ password: text });
  };

  Login = async (email, pass) => {
    var resultCrypto = await Crypto(pass);
    const response = await api.post("/user/validation", {
      email: email,
      senha: resultCrypto
    });

    const { UserId, token, message, SenhaProv, result, error } = response.data;
    if (result && !SenhaProv) {
      await AsyncStorage.setItem("IsLogin", "1");
      await AsyncStorage.setItem("DATA_USER", JSON.stringify(response.data));
      this.props.navigation.navigate("Home");
    } else if (result && SenhaProv) {
      this.props.navigation.navigate("NovaSenha");
    } else if (!result && !error && message === "E-mail ou senha invalidos.") {
      Alert.alert("Ops!", message);
    } else {
      Alert.alert("Ops!", "Não foi possível processar suas informações.");
    }
  };

  render() {
    return (
      // <Background>
      <Container>
        {/* <Image
            style={{ height: 80, width: 80, marginTop: 50 }}
            resizeMode="contain"
            source={require("~/assets/agro.png")}
          /> */}
        {/* <View
            style={{
              flex: 1,
              marginBottom: 30,
              backgroundColor: "#fff",
              height: hp("30%"),
              width: wp("90%"),
              marginTop: 30,
              justifyContent: "center",
              borderRadius: 10,
              shadowColor: "#000000",
              shadowOffset: { width: 0, height: 3 },
              shadowOpacity: 1.0,
              shadowRadius: 5
            }}
          > */}
        {/* <Text
              style={{
                textAlign: "center",
                fontSize: 20,
                color: "#000000",
                fontWeight: "bold"
              }}
            >
              AgroInfo
            </Text> */}
        <Form>
          <FormInput
            icon="user-circle"
            placeholder="E-mail"
            keyboardType="email-address"
            fontSize={12}
            returnKeyType="next"
            autoCapitalize="none"
            autoCorrect={false}
            onSubmitEditing={() => this.senhaInput.focus()}
            onChangeText={this.handleEmail}
          />

          <FormInput
            icon="lock"
            secureTextEntry
            placeholder="Senha"
            fontSize={12}
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={this.handlePassword}
            value={this.state.text}
            ref={senha => (this.senhaInput = senha)}
          />
          <SubmitButton
            onPress={() => this.Login(this.state.email, this.state.password)}
          >
            <Text>Acessar</Text>
          </SubmitButton>
          {/* <SubmitButton>{I18n.t("Login")}</SubmitButton> */}
          <View style={{ flexDirection: "row" }}>
            <SignLink
              onPress={() => this.props.navigation.navigate("Cadastro")}
            >
              <SignLinkText>Criar conta</SignLinkText>
            </SignLink>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "flex-end"
              }}
            >
              <SignLink
                onPress={() => this.props.navigation.navigate("recuperarSenha")}
              >
                <SignLinkText>Esqueci minha senha?</SignLinkText>
              </SignLink>
            </View>
          </View>
          {/* <Text
              style={{
                textAlign: "center",
                fontSize: 15,
                color: "#000",
                marginTop: 20
              }}
            >
              -OU-
            </Text> */}
          <View
            style={{
              alignItems: "center",
              flexDirection: "row",
              justifyContent: "center",
              marginTop: 20
            }}
          >
            {/* <Image
                style={{
                  height: 36,
                  width: 36,
                  marginRight: 20,
                  marginTop: 30
                }}
                resizeMode="contain"
                source={require("~/assets/facebook.png")}
              /> */}
            {/* <Image
                style={{
                  height: 36,
                  width: 36,
                  marginLeft: 20,
                  marginTop: 30
                }}
                resizeMode="contain"
                source={require("~/assets/google.png")}
              /> */}
          </View>
        </Form>
      </Container>
      // </Background>
    );
  }
}
