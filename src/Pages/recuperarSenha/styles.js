import styled from "styled-components/native";
import { Platform } from "react-native";

import Input from "~/Components/Input";
import Button from "~/Components/Button";

export const Container = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.IOS === "ios",
  behavior: "padding"
})`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0 30px;
`;

export const Form = styled.View`
  align-self: stretch;
  margin-top: 50px;
`;

export const FormInput = styled(Input)`
  margin-bottom: 10px;
`;

export const SubmitButton = styled(Button)`
  margin-top: 5px;
`;

// export const SignLink = styled.TouchableOpacity`
//   margin-top: 20px;
//   margin-left: 5px;
//   margin-right: 5px;
// `;
// export const SignLinkText = styled.Text`
//   color: #000;
//   font-weight: bold;
//   align-self: center;
//   align-content: flex-start;
//   align-content: space-between;
// `;
