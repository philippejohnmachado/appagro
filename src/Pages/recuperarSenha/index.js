import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from "react-native";
import {
  Container,
  Form,
  FormInput,
  SubmitButton
  // SignLink,
  // SignLinkText
} from "./styles";

// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp
// } from "react-native-responsive-screen";
import api from "~/services/api";
// import Crypto from "~/utils/crypto";

export default class Cadastro extends Component {
  state = {
    email: ""
  };

  async componentDidMount() {
    this.email = "";
  }

  handleEmail = text => {
    this.setState({ email: text });
  };

  NovaSenha = async email => {
    if (email.length > 0) {
      const response = await api.get("/user/novaSenha/" + email);
      const { message, result, error } = response.data;

      if (!error && result) {
        this.props.navigation.navigate("Login");
      }
    } else {
      Alert.alert("Preecha o e-mail");
    }
  };

  render() {
    return (
      <Container>
        <Text
          style={{
            textAlign: "center",
            fontSize: 20,
            color: "#000",
            fontWeight: "bold",
            marginTop: 10
          }}
        >
          Recuperar senha
        </Text>
        <Text
          style={{
            textAlign: "center",
            fontSize: 12,
            color: "#000",
            marginTop: 10
          }}
        >
          A sua senha provisoria será enviada para o e-mail cadastrado, confirme
          o seu e-mail por favor
        </Text>
        <Form>
          <FormInput
            placeholder="E-mail"
            keyboardType="email-address"
            placeholderTextColor="#000000"
            fontSize={12}
            returnKeyType="go"
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={this.handleEmail}
          />
          <SubmitButton
            onPress={() => this.NovaSenha(this.state.email)}
          >
            <Text>Finalizar</Text>
          </SubmitButton>
        </Form>
      </Container>
    );
  }
}
