import React, { Component } from "react";
import { Dimensions, AsyncStorage, View } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import SideMenu from "~/Components/SideMenu";
// import { metrics } from "~/styles";

// Pages
import Login from "~/Pages/Login";
import Cadastro from "~/Pages/Cadastro";
import Home from "~/Pages/Home";
import NovaSenha from "~/Pages/NovaSenha";
import recuperarSenha from "~/Pages/recuperarSenha";
import Usuario from "~/Pages/Usuario";

const WIDTH = Dimensions.get("window").width;

const DrawerConfig = {
  drawerWidth: WIDTH * 0.83,
  drawerPosition: "left",
  drawerType: "slide",
  contentComponent: SideMenu,
  contentComponent: ({ navigation }) => {
    return <SideMenu navigation={navigation} />;
  }
};

const LoginNavigation = createStackNavigator({
  Login: { screen: Login, navigationOptions: { header: null } },
  Cadastro: { screen: Cadastro, navigationOptions: { header: null } },
  NovaSenha: { screen: NovaSenha, navigationOptions: { header: null } },
  recuperarSenha: {
    screen: recuperarSenha,
    navigationOptions: { header: null }
  }
});

const DrawerNavigator = createDrawerNavigator(
  {
    Home: { screen: Home, navigationOptions: { title: "Home" } },
    Usuario: { screen: Usuario, navigationOptions: { title: "Usuario" } }
  },
  DrawerConfig
);

class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
    this._loadData();
  }

  render() {
    return <View />;
  }

  _loadData = async () => {
    const data = await AsyncStorage.getItem("IsLogin");
    if (data === "1") {
      this.props.navigation.navigate("App");
    } else {
      this.props.navigation.navigate("Auth");
    }
  };
}

const Navigation2 = createStackNavigator({
  DrawerNavigator: {
    screen: DrawerNavigator,
    navigationOptions: { header: null }
  }
});

const Navigation1 = createStackNavigator({
  LoginNavigation: {
    screen: LoginNavigation,
    navigationOptions: { header: null }
  },
  DrawerNavigator: {
    screen: DrawerNavigator,
    navigationOptions: { header: null }
  }
});

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: Navigation2,
      Auth: Navigation1
    },
    {
      initialRouteName: "AuthLoading"
    }
  )
);
