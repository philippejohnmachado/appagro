import axios from 'axios';

const api = axios.create({
    baseURL: 'http://machadoitteste-com-br.umbler.net',
  });
  
  export default api;