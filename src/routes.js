import React from "react";

import SideMenu from "~/Navigation/DrawerNavigator";

const Routes = () => <SideMenu />;

export default Routes;
