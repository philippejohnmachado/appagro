import { Alert } from "react-native";
import CryptoJS from "react-native-crypto-js";

export default decrypt = senha => {
  const key = "oEKQZfRTdbqURXNrn7S1OKojoRN11p4gtFxJ";
  const bytes = CryptoJS.AES.decrypt(senha, `${key}`);
  const originalText = bytes.toString(CryptoJS.enc.Utf8);

  Alert.alert("Decrypt: " + `${originalText}`);

  return `${originalText}`;
};
