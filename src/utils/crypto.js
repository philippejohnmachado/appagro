import CryptoJS from "react-native-crypto-js";

export default crypto = senha => {
  const key = "oEKQZfRTdbqURXNrn7S1OKojoRN11p4gtFxJ";
  const ciphertext = CryptoJS.AES.encrypt(senha, `${key}`).toString();

  return `${ciphertext}`;
};
